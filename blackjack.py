import random

YOUR_HAND = []
DEALER_HAND = []
YOUR_TOTAL = 0
DEALER_TOTAL = 0

COINS = 500
BET = 0
MULTIPLIER = 2
WIN_AMOUNT = 0

# deal 2 cards to you and dealer
def deal(you, dealer):
    for i in range(2):
        you.append(random.randint(1,11))
        dealer.append(random.randint(1, 11))


def get_bets(coins, bet):
    print(f'COINS: {coins}')

    bet = input('Bet: ')
    bet = int(bet)
    if bet <= coins:
        client(YOUR_TOTAL, DEALER_TOTAL, YOUR_HAND, DEALER_HAND, bet, COINS, MULTIPLIER)
    elif bet <= 0:
        print('You are broke')
        exit(0)
    else:
        print('Insufficient funds')
        get_bets(coins, bet)


def client(your_total, dealer_total, you, dealer, bet, coins, multiplier):
    print(f'Your bet is {bet}')
    while True:
        # sum your hand
        your_total = 0

        for i in range(len(you)):
            your_total += you[i]
        # sum dealer hands
        dealer_total = 0

        for i in range(len(dealer)):
            dealer_total += dealer[i]

        if dealer_total == your_total:
            print(f'Your hand: {you}')
            print(f'Dealers hand: {dealer}')
            print('PUSH, NO ONE WINS')
            coins -= bet
            restart(your_total, dealer_total, you, dealer, bet, coins, multiplier)
        elif dealer_total == 21:
            print('DEALER WINS')
            coins -= bet
            restart(your_total, dealer_total, you, dealer, bet, coins, multiplier)
        elif dealer_total > 21:
            print('DEALER BUST, PLAYER WIN')
            coins += bet * multiplier
            restart(your_total, dealer_total, you, dealer, bet, coins, multiplier)

        # player
        if your_total < 21:
            print('-' * 20)
            print(f'You have {you}')
            print(f'Your total is {your_total}')
            print(f'Dealer has {dealer}')
            print(f'Dealers total is {dealer_total}')

            choice = input('Do you want to STAND or HIT?')
            if choice == 'stand':
                print('Player stands')
                # do nothing
            elif choice == 'hit':
                # add card to your hand
                you.append(random.randint(1, 11))
            else:
                print('Not a valid input')
        elif your_total == 21:
            # win
            print('-' * 20)
            print('BLACKJACK')
            coins += bet * multiplier
            restart(your_total, dealer_total, you, dealer, bet, coins, multiplier)
        elif your_total > 21:
            # lose
            print('-' * 20)
            print('BUST')
            coins -= bet
            restart(your_total, dealer_total, you, dealer, bet, coins, multiplier)

        # dealer
        if dealer_total < 17:
            dealer.append(random.randint(1, 11))
        else:
            print('Dealer stands')


def restart(your_total, dealer_total, you, dealer, bet, coins, win_amount):
    print(f'COINS: {coins}')
    if coins <= 0:
        print('You are broke')
        exit()

    newgame = input('Want to play again? (y/no)')
    if (newgame == 'y'):
        your_total = 0
        you.clear()
        dealer.clear()

        deal(YOUR_HAND, DEALER_HAND)
        get_bets(coins, BET)
    elif(newgame == 'n'):
        profit = int(coins - 500)
        print(f'You won {profit}' if profit > 0 else f'You lost {profit}')
        exit(0)


deal(YOUR_HAND, DEALER_HAND)
get_bets(COINS, BET)
